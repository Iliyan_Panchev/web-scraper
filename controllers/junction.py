from fastapi import FastAPI
from controllers.front_end_router import front_router
from services import statistics_service


app = FastAPI(title='Scraper web app', 
              description='''Basic web scraper application used to get various data from dev.bg and plot it via chart.js''',
              version=1.0,
              on_startup=[statistics_service.scheduler.start]
              )

app.include_router(front_router)



