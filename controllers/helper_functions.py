import json
import math
from functools import reduce


def get_percentage(jobs_count: dict) -> dict:
    if not jobs_count:
        return {}
    
    total  = reduce(lambda a, b: int(a) + int(b), jobs_count.values())

    for j, c in jobs_count.items():
        c = int(c) / total * 100
        jobs_count[j] = round(c,2)

    return jobs_count    

    
def parse_jobs_statistics(jobs_count: object, language: str='Python') -> tuple:
    values, dates = [], []

    for job in jobs_count:
        values.append(json.loads(job.data_entry.replace("'", "\""))[language])
        dates.append(job.created.strftime("%d/%m/%Y"))

    return (values, dates)



def paginate_job_listings(job_listings: dict, page, items_per_page) -> list[tuple]:
    start_idx = (page - 1) * items_per_page
    end_idx = start_idx + items_per_page
    job_list = [(job, details) for job, details in job_listings.items()]
    paginated_job_listings = job_list[start_idx:end_idx]

    total_pages = math.ceil(len(job_listings) / items_per_page)

    return (paginated_job_listings, total_pages)