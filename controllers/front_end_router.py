
import json
from fastapi import APIRouter, Query, Request 
from fastapi.templating import Jinja2Templates
from controllers.helper_functions import parse_jobs_statistics, paginate_job_listings

from services import statistics_service, jobs_service

front_router = APIRouter(tags=['Front'])
templates = Jinja2Templates(directory="templates")


@front_router.get('/')
async def homepage_display(request: Request, 
                           itemsToShow: int = Query(10, alias="itemsToShow"), 
                           languagesToShow: str = Query('Python', alias="languagesToShow"), 
                           timeFrame: int = Query(14, alias="timeFrame")
                           ):
    # itemsToShow = int(request.query_params.get("itemsToShow", 10))
    # languagesToShow = request.query_params.get("languagesToShow", 'Python')
    # timeFrame = int(request.query_params.get("timeFrame", 14))

    jobs_count = statistics_service.get_latest_jobs_count()
    jobs = statistics_service.get_latest_statistics(timeFrame)
    edited_jobs_count = dict(list(json.loads(jobs_count.data_entry.replace("'", "\"")).items())[:itemsToShow])

    technology, dates = parse_jobs_statistics(jobs, language=languagesToShow)

    return templates.TemplateResponse("homepage.html", {"request": request, 
                                                        "jobs_count": edited_jobs_count, 
                                                        "technology": technology,
                                                        "dates": dates, 
                                                        "current_language": languagesToShow
                                                        })



@front_router.get('/jobs')
def list_jobs(request: Request, 
              page: int = Query(1, alias="p"), 
              items_per_page: int = Query(15, alias="items_per_page")
              ):
    
    latest_jobs = jobs_service.get_latest_jobs()
    job_listings = json.loads(latest_jobs.data_entry.replace("'", "\""))
    scraping_date = latest_jobs.created.strftime("%d/%m/%Y, %H:%M:%S")
    
    paginated_job_listings, total_pages = paginate_job_listings(job_listings, 
                                                                page, 
                                                                items_per_page)
    return templates.TemplateResponse("jobs_listing.html", 
                                      {"request": request, 
                                       "job_listings": paginated_job_listings,
                                       "listing_date": scraping_date,
                                       "current_page": page,
                                       "items_per_page": items_per_page,
                                       "total_pages": total_pages
                                       })




