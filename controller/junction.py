from fastapi import FastAPI
from controller.front_end_router import front_router
from controller.cv_router import cv_router
from controller.learning_router import learning_router
from fastapi.staticfiles import StaticFiles


app = FastAPI(title='Scraper web app', 
              description='''Basic web scraper application used to get various data from dev.bg and plot it via chart.js''',
              version=1.0,
              )


app.mount("/static/css", StaticFiles(directory="static/css"))
app.mount("/static/html", StaticFiles(directory="static/html"))
# app.mount("/static/script", StaticFiles(directory="static/script"))

app.include_router(front_router)
app.include_router(cv_router)
app.include_router(learning_router)

