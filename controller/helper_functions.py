import datetime
import glob
import json
import math
import os
from functools import reduce
from fastapi.templating import Jinja2Templates
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

templates = Jinja2Templates(directory="static/html")

def clean_download_folder():
    '''
    Custom function that deletes .pdf file types in the download folder \
    so they don't hog the memory.
    '''
    files = glob.glob(os.getcwd() + '../download/*.pdf')
    for f in files:
        os.remove(f)
    print('Download folder emptied.')    

        
def get_percentage(jobs_count: dict) -> dict:
    '''
    Custom function that recieves a dictionary and returns it with job count replaced by it's percentage as per all jobs
    '''
    if not jobs_count:
        return {}
    total  = reduce(lambda a, b: int(a) + int(b), jobs_count.values())

    for j, c in jobs_count.items():
        c = int(c) / total * 100
        jobs_count[j] = f'{c:.2f}'

    return jobs_count    

    
def parse_jobs_statistics(jobs_count: object, language: str='Python') -> tuple:
    '''
    Function parses a single jobs_count json object and returns the specified technology in a linear fashion. \n
    Default value is set to Python.
    '''
    values, dates = [], []

    for job in jobs_count:
        values.append(json.loads(job.data_entry)[language])
        dates.append(job.created.strftime("%d/%m/%Y"))

    return (values, dates)



def paginate(job_listings: dict, page, items_per_page) -> list[tuple]:
    '''
    Custom pagination function that recieves a dictionary with job listings, current page and items per page parameters. \n
    Returns the paginated job listings according to items per page value and total pages.
    '''
    start_idx = (page - 1) * items_per_page
    end_idx = start_idx + items_per_page
    job_list = [(job, details) for job, details in job_listings.items()]
    paginated_job_listings = job_list[start_idx:end_idx]

    total_pages = math.ceil(len(job_listings) / items_per_page)

    return (paginated_job_listings, total_pages)



def error_response(request, message, template):
        '''
        Error response function to reduce boilerplate code.
        '''
        return templates.TemplateResponse(template, 
            {"request": request, "msg": message}
        )


def generate_formated_date():
     '''
     Function accepts no arguments, returns the formated time as d/m/Y H:M:S
     '''
     return datetime.datetime.now().strftime('%d/%m/%Y %H:%M:%S')


def generate_formated_date_pdf():
    '''
    Function accepts no arguments, returns the formated time as d_m_Y_H_M_S. \n
    It is used to generated pdf files with date time as a unique id. \n
    Since naming files is easier done with underscores and not slashes.
    '''
    return datetime.datetime.now().strftime('%d_%m_%Y_%H_%M_%S')


def generate_pdf(results, keyword, folder_path, filename):
    '''
    Function uses the report lab library to generated .pdf files with the generated job listings. \n
    Stamps the pdf with date and time when the information was created.
    '''
    pdf_filename = os.path.join(folder_path, filename)
    doc = SimpleDocTemplate(pdf_filename, pagesize=letter)
    story = []

    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Center', alignment=1))

    title = "Job Listings Filtered by: " + keyword
  
    story.append(Paragraph(title, styles['Title']))
    story.append(Paragraph('Generated on: ' + generate_formated_date(), styles['Title']))
    story.append(Spacer(1, 12))

    for job in results:
        job_info = f"<strong>{job}</strong><br/><br/><a href='{results[job]}'>{results[job]}</a>"
        story.append(Paragraph(job_info, styles['Normal']))
        story.append(Spacer(1, 12))

    doc.build(story)

    return filename

