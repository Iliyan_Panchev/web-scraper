import json
from fastapi import APIRouter, Query, Request
from fastapi.templating import Jinja2Templates
from controller.flashcard_data.oop_data import oop_data
from controller.flashcard_data.dsa_data import dsa_data
from controller.flashcard_data.web_data import web_data
from controller.flashcard_data.interview_prep import prep_data


learning_router = APIRouter(tags=['Learning'])
templates = Jinja2Templates(directory="static/html")



@learning_router.get('/learn')
def load_flashcards(request: Request,
                    category: str = Query(alias="category")):
    '''
    Learning end point that returns the rendered html with a list of dictionaries formatted as {'question':question', 'answer':'answer'}.
    It has one query parameter for the category type, then 2 dictionaries are used to map the category type to the data and category type name.
    '''
    
    
    data_dict = {'oop': oop_data,
                 'dsa': dsa_data,
                 'web': web_data,
                 'prep': prep_data,
                 }
    
    full_name_dict = {'oop': 'Object Oriented Programming',
                      'dsa': 'Data Structures and Algorithms',
                      'web': 'Web Applications',
                      'prep': 'Interview Questions'} # not available
    
    return templates.TemplateResponse('flashcard.html', {"request": request, "flashcard_data" : data_dict[category], "object": full_name_dict[category]})


