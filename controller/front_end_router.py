
import json
from fastapi import APIRouter, Query, Request, BackgroundTasks
from fastapi.templating import Jinja2Templates
from controller.helper_functions import parse_jobs_statistics, paginate
from core.database.models import StatisticsData

from services import statistics_service, jobs_service

front_router = APIRouter(tags=['Front'])
templates = Jinja2Templates(directory="static/html")


@front_router.get('/')
async def homepage_display(request: Request, 
                           backgroundtasks: BackgroundTasks,
                           items_to_show: int = Query(5, alias="itemsToShow"), 
                           languages_to_show: str = Query('Python', alias="languagesToShow"), 
                           time_frame: int = Query(5, alias="timeFrame"),
                           scrape_now: bool = Query(False, alias="scrapeNow"),
                           ):
    '''
    Display the homepage with statistics based on user preferences.

    Args:
        request (Request): The FastAPI request object.
        backgroundtasks (BackgroundTasks): FastAPI background tasks for asynchronous processing.
        items_to_show (int): Number of items to display in the homepage.
        languages_to_show (str): The programming language to display statistics for.
        time_frame (int): The time frame for statistics.
        scrape_now (bool): Flag to trigger scraping statistics if not scraped today.

    Returns:
        TemplateResponse: Template response for the homepage.
    '''
    # itemsToShow = int(request.query_params.get("itemsToShow", 10))
    # languagesToShow = request.query_params.get("languagesToShow", 'Python')
    # timeFrame = int(request.query_params.get("timeFrame", 14))


    # if scrape_now and not statistics_service.scraped_today():
    #     backgroundtasks.add_task(statistics_service.scrape_statistics)
    #     backgroundtasks.add_task(statistics_service.scrape_jobs)
    
    jobs_count: StatisticsData = statistics_service.get_latest_jobs_count()

    if jobs_count is None or not statistics_service.scraped_today():

        return templates.TemplateResponse("no_info_response.html", {"request": request})
    
    jobs = statistics_service.get_latest_statistics(time_frame)
    
    edited_jobs_count = dict(list(json.loads(jobs_count.data_entry).items())[:items_to_show])

    technology, dates = parse_jobs_statistics(jobs, language=languages_to_show)

    return templates.TemplateResponse("homepage.html", {"request": request, 
                                                        "jobs_count": edited_jobs_count, 
                                                        "technology": technology,
                                                        "dates": dates, 
                                                        "current_language": languages_to_show
                                                        })



@front_router.get('/jobs')
def list_jobs(request: Request, 
              page: int = Query(1, alias="page"), 
              items_per_page: int = Query(15, alias="items_per_page"),
              ):
     
    '''
    Display a paginated list of job listings.

    Args:
        request (Request): The FastAPI request object.
        page (int): The current page number for pagination.
        items_per_page (int): Number of items to display per page.

    Returns:
        TemplateResponse: Template response for the job listings page.
    '''
    
    latest_jobs = jobs_service.get_latest_jobs()
    job_listings = json.loads(latest_jobs.data_entry)
    scraping_date = latest_jobs.created.strftime("%d/%m/%Y %H:%M:%S")
    
    paginated_job_listings, total_pages = paginate(job_listings, 
                                                    page, 
                                                    items_per_page)
    
    return templates.TemplateResponse("jobs_listing.html", {"request": request, 
                                                            "job_listings": paginated_job_listings,
                                                            "listing_date": scraping_date,
                                                            "current_page": page,
                                                            "total_pages": total_pages,
                                                            "all_job_listings": job_listings
                                                            })




