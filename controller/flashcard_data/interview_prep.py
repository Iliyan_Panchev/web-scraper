prep_data = [
    {'question': 'What are decorators in Python?', 
     'answer': 'Decorators are a powerful and flexible way to modify or extend the behavior of functions or methods without changing their code. They are applied using the "@" symbol followed by the decorator name above the function definition.'},
    
    {'question': 'What are generators in Python?', 
     'answer': 'Generators are a type of iterable, similar to lists or tuples, but they generate values on-the-fly using a special kind of function called a generator function. They are memory-efficient and allow you to iterate over potentially infinite sequences of data.'},
    
    {'question': 'Difference between list, tuple, dictionary, set?', 
     'answer': 'List: Mutable, ordered collection of elements. Tuple: Immutable, ordered collection of elements. Dictionary: Mutable, unordered collection of key-value pairs. Set: Mutable, unordered collection of unique elements.'},
    
    {'question': 'Python built-in types?', 
     'answer': 'Python has several built-in types, including int, float, str, list, tuple, dict, set, bool, complex, and more. These types provide the foundation for creating and working with data in Python programs.'}
]
