oop_data = [
    {'question': 'Why do we define functions in programming?', 
     'answer': 'Functions in programming are defined for various reasons, including modularity, reusability, \
        abstraction, readability, maintenance, code organization, parameterization, and testing. They allow for \
            the separation of code into manageable and reusable units, enhancing readability, facilitating maintenance, and promoting good software engineering practices.'},
    
    {'question': 'How do we define a function in Python?', 
     'answer': 'In Python, you define a function using the def keyword, followed by the function name and any parameters it takes in parentheses. \
        The function body contains the code to be executed, and the return statement, if used, specifies what the function should output.\
            '},
    
    {'question': 'How to call a function with or without parameters?', 
     'answer': 'To call a function without parameters, use the function name followed by parentheses. \
        To call a function with parameters, provide the values for the parameters inside the parentheses, ensuring they match the function definition.'},
    
    {'question': 'What is the purpose of *args and **kwargs?', 
     'answer': 'These are used when the exact number of arguments for a function is unknown. *args creates a tuple inside the function that can be iterated, and **kwargs creates a dictionary that can be iterated using key-value pairs.'},
    
    {'question': 'What does a function without a return statement return?', 
     'answer': 'A function without a return statement in Python implicitly returns None.'},
    
    {'question': 'How many return statements are allowed per function?', 
     'answer': 'A function in Python can have multiple return statements, but it will only execute the first one encountered during the function execution.\
          Once a return statement is executed, the function exits, and no subsequent return statements will be processed.'},
    
    {'question': 'What is a multidimensional list in Python?', 
     'answer': 'A multidimensional list in Python is a list that contains other lists as its elements, forming a nested structure. \
        This structure allows you to create a grid or matrix-like arrangement of data. In Python, you can create a multidimensional list using nested square brackets.'},
    
    {'question': 'How can we iterate through a multidimensional list?', 
     'answer': 'You can iterate through a multidimensional list in Python using nested loops. \
        The outer loop iterates over the rows, and the inner loop iterates over the elements within each row.\
            You can modify the inner loop to perform operations on each element or use the indices to access and manipulate specific elements within the multidimensional list.'},
    
    {'question': 'How many dimensions can we have?', 
     'answer': 'In Python, the number of dimensions in a list is theoretically unlimited, but practically it is determined by the available memory and system resources. \
        Lists can be nested to create multidimensional structures, and you can have lists of lists to represent higher-dimensional data.\
          However, as the number of dimensions increases, the code readability and the complexity of accessing specific elements also increase. \
            In practice, most applications rarely use more than three dimensions for ease of understanding and maintenance.'},
    
    {'question': 'What is scope?', 
     'answer': 'In programming, the term scope refers to the region of the code where a particular variable or identifier can be accessed or modified. \
        It defines the context in which variables exist and can be used. \
        The scope of a variable is determined by where it is declared or defined within the code. \
            The idea of scope is crucial for understanding how variables are organized and accessed in a program. \
                It helps prevent naming conflicts, as variables in different scopes can have the same name without interfering with each other. \
                The concept of scope is present in most programming languages including, Python, JavaScript and others.'},
    
    {'question': 'What are the types of scope in Python?', 
     'answer': 'Global Scope: Variables defined outside of any function or class are globally scoped. They can be accessed from anywhere in the code. \
        Local Scope: Variables defined within a function have local scope. They are only accessible within that function. \
           Enclosing scope: Variables defined in a nested function, allowing inner functions to access variables from the outer (enclosing) function. \
            Understanding these scopes is crucial for proper variable management and writing modular code. \
                 Built-in scope: Refers to the scope that contains names and identifiers that are built into the Python language itself. \
                    These names are part of the Python standard library and include functions, exceptions, and objects that are available for use without the need for explicit import statements.'},
        
    {'question': 'What is a module in Python?', 
     'answer': 'In Python, a module is a file containing Python definitions and statements. \
        The file name is the module name with the suffix .py appended. \
            A module allows you to organize Python code into reusable files, making it easier to manage and maintain codebases.'},
    
    {'question': 'How can we import a module?', 
     'answer': 'In Python, you can import a module using the import keyword. \
        There are different ways to import modules, allowing you to control how you reference the names defined in the module.'},
    
    {'question': 'How can we import only what we need from a module?', 
     'answer': 'To import only specific names (functions, variables, or classes) from a module in Python, you can use the from ... import ... syntax. '},
    
    {'question': 'What are the differences between set, list, tuple?', 
     'answer': 'Tuple - ordered collection that is immutable, accepts duplication. List - ordered, mutable, accepts duplication. Set - unordered, unique elements, can be iterated with a for-loop.'},
    
    {'question': 'What is a dictionary?', 
     'answer': 'A dictionary is an ordered collection of elements in key-value pairs. It is mutable and does not allow duplication of keys.'},
    
    {'question': 'What is a list comprehension?', 
     'answer': 'List comprehension is a syntax in Python for concise creation and iteration of lists. It returns a list or a dictionary.'},
    
    {'question': 'Write a list comprehension with/without a filter clause.', 
     'answer': 'With filter: even_squares = [x * x for x in range(10) if x % 2 == 0]'},
    
    {'question': 'What is git? What are the benefits of using it?', 
     'answer': 'Git is a distributed version control system created by Linus Torvalds. It tracks changes in the source code during software development, allowing multiple developers to collaborate. Benefits include easy collaboration, version tracking, and code backup.'},
    
    {'question': 'What is GitLab? What are the benefits of using it?', 
     'answer': 'GitLab is a cloud technology for hosting Git repositories, similar to GitHub. Benefits include centralized repository hosting, collaboration tools, and CI/CD pipelines.'},
    
    {'question': 'How is GitLab different from git?', 
     'answer': 'Git is the software used for version control, while GitLab is an online platform that provides hosting for Git repositories, collaboration tools, and CI/CD functionality.'},
    
    {'question': 'Which are the global configurations you need to set in order to use git? What is their role?', 'answer': 'Install Git on the computer, clone a repository if it exists, and set up an email and password for authentication.'},
    
    {'question': 'What is a commit? What information does it contain?', 
     'answer': 'A commit is a snapshot of files at a specific point in time. It contains changes made, a unique identifier (hash), author details, and a timestamp.'},
    
    {'question': 'What is OOP? - What are classes, instances, objects?', 
     'answer': 'Object-Oriented Programming (OOP) is a programming paradigm that organizes software design around objects. A class is a blueprint for creating objects. An instance is a specific occurrence of an object, and an object is an instance of a class.'},
    
    {'question': 'How can we create a class that has a specific state and/or behavior?', 
     'answer': 'Define a class using the class keyword, giving it a Pascal Case name. Create its attributes (state) and methods (behavior).'},
    
    {'question': 'Compare class-level and instance-level attributes?', 
     'answer': 'Class-level attributes are shared among all instances of a class. Instance-level attributes are specific to each instance and do not affect others.'},
    
    {'question': 'What are magic methods?', 
     'answer': 'Magic methods, also known as dunder methods, start and end with double underscores. They are invoked internally by the class under certain circumstances and are not intended to be called directly.'},
    
    {'question': 'Compare mutable and immutable objects?', 
     'answer': 'Mutable objects can be changed after creation (e.g., lists). Immutable objects cannot be changed after creation (e.g., tuples).'},
    
    {'question': 'Why are the benefits of using Type Hints?', 
     'answer': 'Type Hints help with code completion (IntelliSense) and reduce the chance of errors and bugs. While not mandatory, they are recommended for better code quality.'},
    
    {'question': 'What is encapsulation? What is its purpose?', 
     'answer': 'Encapsulation is a fundamental OOP principle. It involves grouping data along with the methods that operate on the data. Its purpose is to protect the internal state of an object and to hide implementation details.'},
    
    {'question': 'What is a property in Python?', 'answer': 
     'A property is a special decorator used when we want to hide information from being changed outside the class. It involves the use of getter, setter, and deleter methods.'},
    
    {'question': 'How can we declare a readonly property?', 
     'answer': '@property decorator can be used to declare a read-only property by defining only the getter method.'},
    
    {'question': 'What is Inheritance? What is its purpose?', 
     'answer': 'Inheritance is a fundamental OOP concept that allows a class to inherit properties and behaviors from another class. Its purpose is to reduce code duplication and create an is-a relationship between classes.'},
    
    {'question': 'What is Polymorphism? What is its purpose?', 
     'answer': 'Polymorphism is a fundamental OOP concept that allows objects of different classes to be treated as objects of a common base class. Its purpose is to enable code to work with objects of multiple types in a unified way.'},
    
    {'question': 'What is Abstraction? What is its purpose?', 
     'answer': 'Abstraction is a fundamental OOP principle that involves simplifying complex systems by modeling classes based on the essential features. Its purpose is to focus on what an object does rather than how it does it.'},
    
    {'question': 'How do we inherit from a class in Python?', 
     'answer': 'Use the syntax: class NewClass(BaseClass). Classes are named with Pascal Case naming convention.'},
    
    {'question': 'What is the purpose of the super().__init__()', 
     'answer': 'super().__init__() is used to call the constructor of the parent class. It is necessary to initialize the parent class object in the child class. It allows adding specific information to the child object as well.'},
    
    {'question': 'How do we override an inherited method?', 
     'answer': 'Override a method by replacing its body in the child class. The method name and arguments must be the same as in the base class.'},
    
    {'question': 'How do we override an inherited property?', 
     'answer': 'Override an inherited property by defining a new getter or setter in the child class.'},
    
    {'question': 'What is Duck Typing? Compare with Static Typing?', 
     'answer': 'Duck Typing is a concept in dynamic typing languages where the type of an object is less important than the methods it defines. Static Typing involves specifying a variable type at compile time, ensuring type safety.'},
    
    {'question': 'Benefits and limitations of Duck Typing?', 
     'answer': 'Benefits include flexibility and concise code. Limitations include error-proneness as errors are discovered at runtime.'},
    
    {'question': 'How do we achieve Multiple Inheritance in Python? Benefits?', 'answer': 'Achieve multiple inheritance by defining a class that inherits from multiple classes. Benefits include code reuse from multiple sources.'},
    
    {'question': 'When to avoid Multiple Inheritance?', 
     'answer': 'Avoid multiple inheritance when there is a lot of similar methods/attributes or a diamond structure is formed.'},
    
    {'question': 'What is the MRO of a class in Python?', 
     'answer': 'The Method Resolution Order (MRO) determines the sequence in which base classes are searched when looking for a method or attribute.'},
    
    {'question': 'How do we handle exceptions?', 
     'answer': 'Handle exceptions using try/except blocks. Code within the try block is executed, and if an exception occurs, the except block is executed. An optional else block is executed if no exception occurs, and finally block is always executed.'},
    
    {'question': 'How do we raise exceptions?', 
     'answer': 'Raise an exception using the raise statement, followed by the exception instance. It is often used to indicate errors in code.'},
    
    {'question': 'When is a good time to raise an exception?', 
     'answer': 'Raise an exception when an error occurs that should be handled by the calling code or when there is a need to protect the valid state of a class.'},
    
    {'question': 'What is a unit and how do we test it?', 
     'answer': 'A unit is a small piece of code with a specific functionality. It can be tested by creating unit tests that check if the unit works as expected.'},
    
    {'question': 'What are the qualities of a good unit test?', 
     'answer': 'A good unit test is isolated, repeatable, self-verifying, and has high code coverage. It should be able to run independently of other tests.'},
    
    {'question':'When should we not write unit tests?', 
     'answer':'If the writing of the test itself is harder than the code.If the code is throw-away or placeholder code. If there is any doubt, test.'},
    
    {'question':'What is the AAA pattern and how does it help in test quality?', 
     'answer':'The AAA pattern, which stands for Arrange, Act, Assert, is a testing pattern commonly used in unit testing to structure and organize test cases. This pattern helps improve the clarity, readability, and maintainability of tests, contributing to overall test quality.'},

    {'question': 'When do we need to store data in a file?', 
     'answer': 'Usually when we code data is stored in the RAM, after the program ends all data is lost. Therefore, we can write on files and have our commands saved in a state for later. Files are read/write.'},

    {'question': 'What is a relative file path? How does it differ from an absolute one?', 
     'answer': 'An absolute path is defined as specifying the location of a file or directory from the root directory(/). In other words, we can say that an absolute path is a complete path from the start of the actual file system from the / directory. A relative path is defined as the path related to the present working directory (pwd).'},

    {'question': 'What is a stream?', 
     'answer': "The 'open' function returns the file as a TextIOWrapper object, abstract for stream. A stream is a sequence of values usually processed one by one. Reading a stream exhausts it."},

    {'question': 'How can we read a text file in Python?', 
     'answer': 'python f = open(the-zen-of-python.txt, r) f.close() # or with open(the-zen-of-python.txt, r) as f:     f.read() # reads everything    f.readlines() # reads line by line'},

    {'question': 'How can we manipulate files?', 
     'answer': 'We can create, read, write, and overwrite them.'},

    {'question': 'What is a First-Class and Higher-Order function?', 
     'answer': 'When you say that a language has first-class functions, it means that the language treats functions as values – that you can assign a function into a variable, pass it around etc. Higher-order functions take functions as arguments or return functions.'},

    {'question': 'What are the benefits of using an anonymous function?', 
     'answer': 'When we need to use a function only once in our code. This can greatly simplify programs, as often calculations are very simple and the use of anonymous functions reduces the number of code files necessary for a program.'},

    {'question': 'What are the usages of the map and filter functions?', 
     'answer': 'python # map() map(fun, iter) # returns an iterator of the results after applying the given function to each item of a given iterable.# filter()filter(fun, iter) # returns an iterator where the items are filtered through a function to test if the item is accepted or not.'},

    {'question': 'What is the reduce function?', 
     'answer': 'The reduce(fun, iter) function is used to apply a particular function passed in its argument to all of the list elements mentioned in the sequence passed along. This function is defined in the “functools” module.'}

    ]
