dsa_data = [
    {'question': 'Explain what a linear data structure is.', 
     'answer': 'A linear data structure is one in which data items are ordered sequentially or linearly, with each member attached to its previous and next neighboring elements. Since the data elements are stored in a linear fashion, the structure allows single-level data storage.'},

    {'question': 'Explain what an array is.', 
     'answer': 'An array is a data structure consisting of a collection of elements (values or variables), each identified by at least one array index or key. Depending on the language, array types may overlap (or be identified with) other data types that describe aggregates of values, such as lists and strings.'},

    {'question': 'Explain what a linked list is.', 
     'answer': 'In computer science, a linked list is a linear collection of data elements whose order is not given by their physical placement in memory. Instead, each element points to the next. It is a data structure consisting of a collection of nodes which together represent a sequence.'},

    {'question': 'Explain what stack and queue are.', 
     'answer': 'Stack is a container of objects that are inserted and removed according to the last-in, first-out (LIFO) principle. Queue is a container of objects (a linear collection) that are inserted and removed according to the first-in, first-out (FIFO) principle.'},

    {'question': 'What is array - how are elements stored?', 
     'answer': 'An array is a linear data structure that collects elements of the same data type and stores them in contiguous and adjacent memory locations. Arrays work on an index system starting from 0 to (n-1), where n is the size of the array.'},

    {'question': 'What is a linked list - how are elements stored?', 
     'answer': 'A linked list is a data structure used in computer science to organize and store elements in a linear sequence. Unlike arrays, linked lists do not require contiguous memory locations for their elements. \
     Instead, elements in a linked list are stored in nodes, and each node contains two fields: Data/Value field and Next Pointer/Reference'},

    {'question': 'What is a dynamic array?', 
     'answer': 'A dynamic array can be constructed by allocating an array of fixed-size, typically larger than the number of elements immediately required. The elements of the dynamic array are stored contiguously at the start of the underlying array, and the remaining positions towards the end of the underlying array are reserved or unused.'},

    {'question': 'What are the running times of common operations in an array?', 
     'answer': 'O(n) (linear time complexity).'},

    {'question': 'What are the running times of common operations in a linked list?', 
     'answer': 'In a singly linked list, the time complexity for inserting and deleting an element from the list is O(n). In a doubly-linked list, the time complexity for inserting and deleting an element is O(1).'},
    
    {'question': 'How would you compare array with linked list?', 
     'answer': 'Elements in an array are grouped together in RAM, while elements of linked lists are scattered in RAM. Also, references take extra memory.'},

    {'question': 'What are stacks and queues and what are they useful for?', 
     'answer': 'Stacks and queues are foundational data structures useful for adding and removing elements in specific orders. They are crucial in depth-first-search and breadth-first-search algorithms for graph traversal.'},

    {'question': 'What is a hash table - how do we store elements?', 
     'answer': 'In a hash table, data is stored in an array format, where each data value has its own unique index value. Access of data becomes very fast if we know the index of the desired data. It becomes a data structure in which insertion and search operations are very fast, irrespective of the size of the data.'},

    {'question': 'What is the complexity of common hash table operations?', 
     'answer': 'In hashing, all the above operations can be performed in O(1) (constant time). The worst-case time complexity for hashing remains O(n), but the average case time complexity is O(1). Chaining is O(n) since we have to iterate over the members of the chain.'},

    {'question': 'What is a hashing algorithm?', 
     'answer': 'A hashing algorithm is a mathematical function that garbles data and makes it unreadable. Hashing algorithms are one-way programs, so the text can\'t be unscrambled and decoded by anyone else. And that\'s the point.'},
     {'question': 'What is a collision? How can we resolve collisions?', 
     'answer': 'In computer science, a hash collision or hash clash is when two pieces of data in a hash table share the same hash value. Collisions can be resolved by techniques like chaining or open addressing.'},

    {'question': 'What are the important characteristics of a set?', 
     'answer': 'Unordered collection, no indexing, no duplicates, can be iterated over with a for loop. The `issubset()` method returns whether another set contains this set or not.'},

    {'question': 'Give examples of algorithms which we can improve by using a set?', 
     'answer': 'Remove duplicates, find if a set is a part of another set.'},

    {'question': 'What are the important characteristics of a dictionary?', 
     'answer': 'Ordered collection, no duplicates for keys allowed, key:value pairs. Based on a hash table, based on an array. Find value by key with O(1) time complexity.'},

    {'question': 'Give examples of algorithms which we can improve by using a dictionary?', 
     'answer': 'Searching by key, grouping of data together, frequency counting, caching'},

    {'question': 'What is recursion?', 
     'answer': 'Recursion is a method of solving a computational problem where the solution depends on solutions to smaller instances of the same problem. Recursive functions call themselves from within their own code.'},

    {'question': 'What is the base case in a recursive algorithm?', 
     'answer': 'The base case is the condition to stop the recursion. Recursive functions have two portions: the base case and the recursive case. The base case defines when the recursion should stop.'},

    {'question': 'What are the pros and cons of recursive algorithms?', 
     'answer': 'Advantages include reduced time complexity, relaxation on the number of iterations, and ease of implementation. Disadvantages include the potential for a StackOverflowException due to memory consumption.'},

    {'question': 'How does linear search work? What is its complexity?', 
     'answer': 'Linear search examines each item in a data set until a match is found. Complexity is O(n), where n is the size of the data set.'},

    {'question': 'How does binary search work? What is its complexity?', 
     'answer': 'Binary search works on a sorted collection. It starts from the middle element and narrows down the search based on the comparison. Complexity is O(log n).'}, 

    {'question': 'What is divide and conquer? Give examples.', 
     'answer': 'Divide and conquer is an algorithm design paradigm that recursively breaks down a problem into smaller sub-problems. Examples include merge sort and quicksort.'},

    {'question': 'How does Merge Sort work?', 
     'answer': 'Merge sort divides a list into halves, iterates through the halves, and combines solutions to sub-problems to get the final sorted list.'},

    {'question': 'How does Quick Sort work?', 
     'answer': 'Quicksort breaks an array into smaller ones, swaps them based on a pivot element, and repeats until the entire array is sorted.'},

    {'question': 'How does Counting Sort work?', 
     'answer': 'Counting sort determines the positions of each key value in the output sequence by counting the number of objects with distinct key values and applying prefix sum to those counts.'},

    {'question': 'What is a Binary Tree?', 
     'answer': 'In computer science, a binary tree is a tree data structure in which each node has at most two children, referred to as the left child and the right child.'},

    {'question': 'What is BFS? Why would we use it for?', 
     'answer': 'Breadth-first search (BFS) is an algorithm for searching a tree data structure for a node that satisfies a given property. It is used when searching for the shortest path in unweighted graphs.'},

    {'question': 'What is the difference between PostOrder, PreOrder, and InOrder DFS traversal?', 
     'answer': 'Inorder traverses from the left subtree to the root then to the right subtree. Preorder traverses from the root to the left subtree then to the right subtree. Postorder traverses from the left subtree to the right subtree then to the root.'},

    {'question': 'What is a Binary Search Tree?', 
     'answer': 'In computer science, a binary search tree (BST) is a rooted binary tree data structure where the key of each internal node is greater than all keys in its left subtree and less than the keys in its right subtree.'}
     ]
