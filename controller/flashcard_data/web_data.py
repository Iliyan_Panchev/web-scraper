web_data = [
    {
        "question": "What are databases and why do we need them?",
        "answer": "A database is a structured collection of data that is organized in a way that allows for efficient storage, retrieval, and manipulation of information. Databases are used to store information for a wide range of applications, including business, finance, healthcare, education, and many others."
    },
    {
        "question": "What are the different types of databases?",
        "answer": "Relational database, NoSQL database, Object-oriented database, Hierarchical database, Graph database."
    },
    {
        "question": "What are relational databases?",
        "answer": "A relational database is a type of database that stores and organizes data in tables with relationships between them. It is based on the relational model of data management, introduced by Edgar F. Codd in 1970. Relational databases use SQL for data retrieval and manipulation."
    },
    {
        "question": "What are primary and unique keys and how are they different?",
        "answer": "A primary key is a key that uniquely identifies each record in a table and cannot store NULL values. A unique key prevents duplicate values in a column and can store NULL values."
    },
    {
        "question": "What are one-to-one relations and how are they realized?",
        "answer": "One-to-one relations occur when each record in one table is related to only one record in another table. They are realized by linking the primary key of one table to the primary key of another table."
    },
    {
        "question": "What are one-to-many and many-to-one relations and how are they realized?",
        "answer": "One-to-many and many-to-one relations occur when each record in one table can be related to multiple records in another table. They are realized by linking the primary key of the 'one' table to the foreign key of the 'many' table."
    },
    {
        "question": "What are many-to-many relations and how are they realized?",
        "answer": "Many-to-many relations occur when each record in one table can be related to multiple records in another table, and vice versa. They are realized by introducing a junction table with foreign keys from both tables."
    },
    {
        "question": "What are foreign keys and why do we need them?",
        "answer": "A foreign key is a column or combination of columns that is used to establish and enforce a link between the data in two tables to control the data that can be stored in the foreign key table."
    },
    {
        "question": "What is SQL?",
        "answer": "SQL stands for Structured Query Language. It is a programming language used for managing and manipulating relational databases. SQL allows users to query, insert, update, and delete data from a database."
    },
    {
        "question": "What clause do we use to filter the number of columns?",
        "answer": "The SELECT clause is used to specify which columns should be included in the query results and can be used to filter the number of columns returned."
    },
    {
        "question": "What clauses can we use to filter the number of rows returned?",
        "answer": "Clauses like WHERE, HAVING, LIMIT, and OFFSET can be used to filter the number of rows returned in a SQL query."
    },
    {
        "question": "How can we join the records from two or more tables?",
        "answer": "To join records from two or more tables, we can use the JOIN clause. Types of joins include INNER JOIN, LEFT JOIN, RIGHT JOIN, and FULL OUTER JOIN."
    },
    {
        "question": "How can we aggregate values from multiple rows?",
        "answer": "Aggregate functions such as SUM, AVG, MIN, MAX, and COUNT can be used to aggregate values from multiple rows in a SQL query."
    },
    {
        "question": "What is HTTP?",
        "answer": "HTTP (Hypertext Transfer Protocol) is a protocol used for communication between web servers and web clients (such as web browsers) over the internet. It is the foundation of data communication on the world wide web."
    },
    {
        "question": "What is the client-server model?",
        "answer": "The client-server model is a computing architecture in which a client requests a service or resource from a server, which provides the requested service or resource. The client is typically a user-facing application, while the server provides the necessary computing resources to support the client's request."
    },
    {
        "question": "What is the request-response model?",
        "answer": "The request-response model is a communication pattern in which a client sends a request to a server, and the server responds with a message. This model is used extensively in HTTP, where a client sends an HTTP request to a server, and the server sends an HTTP response back to the client."
    },
    {
        "question": "What are HTTP methods, headers, and status codes?",
        "answer": "HTTP methods are verbs that indicate the intended action to be performed on a resource, such as GET, POST, PUT, and DELETE. HTTP headers are additional information sent with an HTTP request or response, used for various purposes. HTTP status codes are three-digit codes that indicate the status of an HTTP response, such as 200 OK, 404 Not Found, or 500 Internal Server Error."
    },
    {
        "question": "What is the role of the JSON data format?",
        "answer": "JSON (JavaScript Object Notation) is a lightweight data interchange format used for transmitting structured data between a server and a client. It is easy for humans to read and write and easy for machines to parse and generate."
    },
    {
        "question": "What is the difference between FastAPI and Uvicorn?",
        "answer": "FastAPI is a web framework for building APIs with Python, while Uvicorn is a high-performance web server for running ASGI applications, including FastAPI. FastAPI is the framework, and Uvicorn is the server that runs the FastAPI application."
    },
    {
        "question": "What is a Web Service?",
        "answer": "A web service is a software system that enables interoperability between different applications over a network. It uses standardized protocols such as HTTP, XML, and JSON for communication and data exchange."
    },
    {
        "question": "What is a RESTful Web Service?",
        "answer": "A RESTful web service follows the principles of Representational State Transfer (REST) architectural style. It uses HTTP requests to retrieve, create, update, or delete data resources. It is stateless and relies on standard HTTP methods for communication."
    },
    {
        "question": "How are HTTP and REST different?",
        "answer": "HTTP is a protocol used for communication between a client and server over the web. REST is an architectural style that provides guidelines for building web services. RESTful web services use HTTP as the underlying protocol, but not all HTTP-based services follow REST principles."
    },
    {
        "question": "What is it to be 'stateless'?",
        "answer": "Being 'stateless' means that the server does not maintain any information about the state of the client between requests. Each request contains all the information necessary for the server to process it, promoting scalability and simplicity."
    },
    {
        "question": "What is an endpoint?",
        "answer": "An endpoint is a URL representing a specific resource or service provided by a web service. It is the location where a client can access or interact with a particular functionality."
    },
    {
        "question": "Why do we want to split our applications into layers?",
        "answer": "We split applications into layers for reasons such as separation of concerns, code reuse, and testing. Each layer has a specific responsibility, making the application more organized, maintainable, and scalable."
    },
    {
        "question": "What is the role of the presentation layer?",
        "answer": "The presentation layer, also known as the user interface (UI) layer, is responsible for presenting data to the user and receiving input from the user. It provides a way for users to interact with the application."
    },
    {
        "question": "Where does all core application logic belong?",
        "answer": "All core application logic belongs in the business logic layer, also known as the application logic layer or the domain layer. It implements the business rules, algorithms, and data manipulation logic of the application."
    },
    {
        "question": "Authentication vs Authorization?",
        "answer": "Authentication is the process of verifying the identity of a user, system, or application. Authorization is the process of granting or denying access to a resource based on the user's identity and permissions."
    },
    {
        "question": "Explain a simple way to authenticate a web client?",
        "answer": "A simple way to authenticate a web client is by requiring the user to provide login credentials (e.g., username and password). The server verifies these credentials against a database of registered users."
    },
    {
        "question": "Explain how we can authorize based on roles?",
        "answer": "Authorization based on roles involves granting access to resources based on the user's role or level of permissions. Users are associated with roles, and access is granted or denied based on their roles."
    },
    {
        "question": "What is a JWT? How are they protected against tampering?",
        "answer": "JWT (JSON Web Tokens) is a standard format for securely representing claims between two parties. They are protected against tampering by using a signature, often generated using a secret key or a public/private key pair."
    },
    {
        "question": "What is dependency injection?",
        "answer": "Dependency injection is a software design pattern that separates the creation and management of dependencies from the components that use them. It promotes loose coupling and modular, testable code."
    },
    {
        "question": "What types of dependency injection do you know?",
        "answer": "There are three main types of dependency injection: constructor injection, setter injection, and interface injection."
    },
    {
        "question": "What is a mock object and why do we use it?",
        "answer": "A mock object is a test double that simulates the behavior of a real object in a controlled way. It is used in testing to isolate a unit under test from its dependencies, allowing for more efficient and reliable testing."
    },
    {
        "question": "What is isolation?",
        "answer": "Isolation is the process of separating a unit under test from its dependencies to test it in a controlled environment. It removes the impact of external factors on the unit's behavior."
    },
    {
        "question": "What should we isolate and what should we not?",
        "answer": "We should isolate external dependencies (e.g., databases) but not the unit under test itself. Isolating the unit under test allows for controlled testing, while isolating external dependencies ensures test reliability."
    },
    {
        "question": "Why do we need isolation?",
        "answer": "We need isolation to test a unit's behavior in a controlled and predictable environment, identify issues efficiently, and ensure code reliability and maintainability over time."
    }
]
