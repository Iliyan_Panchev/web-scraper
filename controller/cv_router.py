import os
import time
import PyPDF2
from fastapi import APIRouter, Request, UploadFile
from fastapi.responses import FileResponse
from fastapi.templating import Jinja2Templates
from controller.helper_functions import error_response, generate_formated_date, generate_pdf, generate_formated_date_pdf
from services.cv_service import LargeLanguageModelResponse



cv_router = APIRouter(tags=['CV'])
templates = Jinja2Templates(directory="static/html")


@cv_router.get("/upload")
def load_upload_cv_page(request: Request):
    '''
    Render the upload CV page.

    Args:
        request (Request): The FastAPI request object.

    Returns:
        TemplateResponse: Template response for the upload CV page.
    '''
    return templates.TemplateResponse('upload_cv.html', {"request": request})


@cv_router.get("/download/{pdf_file}")
async def download_pdf(pdf_file):
    '''
    Download a generated PDF file.

    Args:
        pdf_file (str): The name of the PDF file to download.

    Returns:
        FileResponse: File response for downloading the specified PDF file.
    '''

    return FileResponse(path=f"download/{pdf_file}")


@cv_router.post("/upload")
async def upload_file(file: UploadFile, request: Request):
    '''
    Process and filter CV text from an uploaded PDF file.

    Args:
        file (UploadFile): The uploaded PDF file.
        request (Request): The FastAPI request object.

    Returns:
        TemplateResponse: Template response for displaying filtered job results.
    '''
    
    if file.content_type != "application/pdf":
        return error_response(request, "Unsupported file type: Please upload a PDF file.", "upload_cv.html")
    elif file.size > 200000:
        return error_response(request, "Files bigger than 200 kb are not supported.", "upload_cv.html")
    
    pdf_text = ""

    pdf_reader = PyPDF2.PdfReader(file.file)
    for page_num in range(len(pdf_reader.pages)): #if the CV has more than one page, iterate over them
        cv_page = pdf_reader.pages[page_num]
        pdf_text += cv_page.extract_text()
    
    result, search_by_keyword = LargeLanguageModelResponse.compute(pdf_text)

    pdf_folder = "download"  #path to the folder where you want to save the file
    pdf_filename = f"filtered_results_{generate_formated_date_pdf()}.pdf"  #save the pdf file with name and date as unique id
    
    pdf_filepath = generate_pdf(result, search_by_keyword, pdf_folder, pdf_filename)
    
    return templates.TemplateResponse('cv_filter_results.html', {"request": request,
                                                                 "filtered_jobs": result,
                                                                 "date_generated": generate_formated_date(),
                                                                 "keyword": search_by_keyword,
                                                                 "downloadable_file": pdf_filepath
                                                                 })





