import datetime
import json
import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
from time import sleep


BASE_URL = 'https://web-scraper-dev-bg-638e021e2a58.herokuapp.com'
WAIT_TIME = 10


options = Options()
options.add_argument('--headless=new')
driver = webdriver.Chrome(options=options)

class TestFrontPage:
    def test_is_app_online(self):
        response = requests.get(f'{BASE_URL}/jobs')
        assert response.status_code == 200


    def test_job_listings_button_opens_listings(self):
        driver.get(BASE_URL)
        
        button = driver.find_element(By.CLASS_NAME, 'listings-button-jobs')

        button.click()
        
        assert driver.title == 'Job Listings'


    def test_upload_cv_button_opens_right_page(self):
        driver.get(BASE_URL)
       
        button = driver.find_element(By.CSS_SELECTOR, '.listings-button-jobs+ .listings-button-jobs')

        button.click()
       
        assert driver.title == 'Upload CV'


    def test_if_listings_links_lead_to_homepage(self):
        driver.get(f'{BASE_URL}/jobs')
        button = driver.find_element(By.CLASS_NAME, 'homepage-button')  
        button.click()
     
        assert driver.title == 'Job Hunter'


    def test_dynamic_text_pie_chart(self):
        COUNT = 5
        driver.get(BASE_URL)


        dropdown = Select(driver.find_element(By.CSS_SELECTOR, '#itemsToShow'))
        dropdown.select_by_visible_text(str(COUNT))

        
        button = driver.find_element(By.CSS_SELECTOR, '.show-data-button-2')  
        button.click()

        dynamic_text = driver.find_element(By.CSS_SELECTOR, '#dynamic-text-technologies')


        assert f"Pie chart with job distribution for {COUNT} languages and technologies." == dynamic_text.text


    def test_dynamic_text_line_chart(self):
        PERIOD = 5
        LANGUAGE = 'Python'

        driver.get(BASE_URL)
      
       
        dropdown_time = Select(driver.find_element(By.CSS_SELECTOR, '#timeFrame'))
        dropdown_time.select_by_visible_text('5')
        dropdown_language = Select(driver.find_element(By.CSS_SELECTOR, '#languagesToShow'))
        dropdown_language.select_by_visible_text('Python')
    
        
        button = driver.find_element(By.CSS_SELECTOR, '.show-data-button-1')  
        button.click()
   
        dynamic_text = driver.find_element(By.CSS_SELECTOR, '#dynamic-text-statistics')
      

        assert f"Line chart displaying the job listings available for {LANGUAGE} for the last {PERIOD} days." == dynamic_text.text
       
    def test_upload_button_when_no_file(self):
        driver.get(BASE_URL + '/upload')

        header = driver.find_element(By.CSS_SELECTOR, 'body > h1')
        upload_button = driver.find_element(By.CSS_SELECTOR, 'body > form > input[type=submit]:nth-child(4)')

        upload_button.click()

        assert driver.title == 'Upload CV'
        assert header.text == 'Get job listings tailored to your qualifications.'
    

    def test_upload_cv_disclaimer_text_reads(self):

        driver.get(BASE_URL + '/upload')

        disclaimer = driver.find_element(By.CSS_SELECTOR, 'body > form > p')

        assert disclaimer.text == 'Disclaimer: No information from the CV is retained in the application.'


class TestDropDownMenus:
    def test_dropdown_menu_languages(self):
        driver.get(BASE_URL)
        dropdown = Select(driver.find_element(By.ID, 'languagesToShow'))

        languages = ['Java', 'Python', 'QA Automation', 'PHP', 'React', 'Node.js', 'Go', '.NET']

        for language in languages:
            dropdown.select_by_visible_text(language)
            selected_option = dropdown.first_selected_option
          
            assert selected_option.text == language

        assert driver.title == 'Job Hunter'



    def test_languages_display_count(self):
        driver.get(BASE_URL)
        dropdown = Select(driver.find_element(By.ID, 'timeFrame'))

        time_frames = ['5', '7', '14', '21', '30', '60', '90', '180']

        for option in time_frames:
            dropdown.select_by_visible_text(option)
            selected_option = dropdown.first_selected_option
            
            assert selected_option.text == option

        assert driver.title == 'Job Hunter'


    def test_technologies_display_count(self):
        driver.get(BASE_URL)
        dropdown = Select(driver.find_element(By.ID, 'itemsToShow'))

        item_counts = ['5', '10', '15', '20', '25', '30']

        for option in item_counts:
            dropdown.select_by_visible_text(option)
            selected_option = dropdown.first_selected_option
            
            assert selected_option.text == option

        assert driver.title == 'Job Hunter'








