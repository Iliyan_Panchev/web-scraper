import datetime
import pytest
from controller.helper_functions import get_percentage, paginate, parse_jobs_statistics
from unittest.mock import Mock

@pytest.fixture
def mock_database_results():
    return [
        Mock(data_entry='{"Python": 10}', created=datetime.datetime(2023, 9, 1)),
        Mock(data_entry='{"Python": 15}', created=datetime.datetime(2023, 9, 2)),
        Mock(data_entry='{"Python": 20}', created=datetime.datetime(2023, 9, 3)),
    ]

class TestGetPercentage:
    def test_get_percentage(self):
        #Arrange
        tested = {'Job1': 11, 'Job2': 11, 'Job3': 23}
        expected_results = {'Job1': '24.44', 'Job2': '24.44', 'Job3': '51.11'}
        #Act
        results = get_percentage(tested)
        #Assert
        assert results == expected_results


    def test_get_percentage_empty_data(self):
        tested = {}
        expected_results = {}
        #Act
        results = get_percentage(tested)
        #Assert
        assert results == expected_results


class TestPaginateJobsListings:
    def test_paginate_job_listings(self):
        #Arrange
        tested = {'job1':'link','job2':'link', 'job3':'link'}
        expected_results_one = ([('job1', 'link'), ('job2', 'link')], 2)
        expected_results_two = ([('job3', 'link')], 2)
        #Act
        results_page_one = paginate(tested, 1, 2)
        results_page_two = paginate(tested, 2, 2)
        #Assert
        assert expected_results_one == results_page_one
        assert expected_results_two == results_page_two


    def test_paginate_job_listings_empty_data(self):
        #Arrange
        tested = {}
        expected_results_one = ([], 0)
        expected_results_two = ([], 0)
        #Act
        results_page_one = paginate(tested, 1, 2)
        results_page_two = paginate(tested, 2, 2)
        #Assert
        assert expected_results_one == results_page_one
        assert expected_results_two == results_page_two


class TestParseJobsStatistics:
    def test_parse_jobs_statistics(self, mock_database_results):
        #Arrange
        expected_values = [10, 15, 20]
        expected_dates = ['01/09/2023', '02/09/2023', '03/09/2023']
        #Act
        values, dates = parse_jobs_statistics(mock_database_results)
        #Assert
        assert values == expected_values
        assert dates == expected_dates


    def test_parse_jobs_statistics_empty_data(self):
        #Arrange
        mock_jobs = []
        #Act
        values, dates = parse_jobs_statistics(mock_jobs, language='Python')
        #Assert
        assert values == []
        assert dates == []


class TestCleanDownloads:

    def test_clean_download_folder_when_file_exists(self):
        ...