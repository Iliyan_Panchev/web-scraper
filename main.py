import uvicorn
from controller.junction import app


if __name__ == '__main__':
    uvicorn.run('main:app', reload=True)