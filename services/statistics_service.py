import json
from datetime import date
from core.database.db_connection import delete_data, insert_data, get_records, get_last_record
from core.database.models import StatisticsData
from core.database.utilities import INFO_TEMPLATE, get_current_time
from services import scraper_service, jobs_service



def create_new_entry(data: str):
    """
    Create a new entry in the StatisticsData table.

    Args:
        data (str): JSON-formatted data to be stored in the database.

    Returns:
        str: A success message.
    """
    data_entry = StatisticsData(data_entry=data, created=get_current_time())
    insert_data(data_entry)

    return f'Successfuly created new entry!'
    

def delete_entry_by_id(id: int):
    """
    Delete an entry from the StatisticsData table by its ID.

    Args:
        id (int): The ID of the entry to be deleted.

    Returns:
        str: A success message.
    """
    delete_data(id, StatisticsData)
    
    return f'Successfuly deleted entry {id}.'


def scrape_statistics():
    """
    Scrape job statistics from the front page of Dev.bg and store the data in the database.
    """
    new_data = scraper_service.ScrapeDevBgFrontPage.get_job_statistics()
    new_data = json.dumps(new_data)
    create_new_entry(new_data)
    print(INFO_TEMPLATE + f'Job numbers scraped at {get_current_time().strftime("%d/%m/%Y, %H:%M:%S")}')


def scrape_jobs():
    """
    Scrape job listings from Dev.bg and Jobs.bg, merge the data, and store it in the database.
    """
    dev_bg_data = scraper_service.ScrapeJobListingsDevBg.get_all_pages(search='junior python')
    jobs_bg_data = scraper_service.ScrapeJobListingsJobsBg.get_all_pages(search='junior python')
    dev_bg_data.update(jobs_bg_data if jobs_bg_data else {'No data from Jobs.bg': 'No data from Jobs.bg'})
    dev_bg_data = json.dumps(dev_bg_data)
    jobs_service.create_new_entry_jobs(dev_bg_data)
    print(INFO_TEMPLATE + f'Jobs scraped at {get_current_time().strftime("%d/%m/%Y, %H:%M:%S")}')


def get_latest_statistics(n):
    """
    Retrieve the latest n records from the StatisticsData table.

    Args:
        n (int): The number of records to retrieve.

    Returns:
        list: A list of StatisticsData records.
    """
    return get_records(n, StatisticsData)


def get_latest_jobs_count():
    """
    Retrieve the latest record from the StatisticsData table.

    Returns:
        StatisticsData: The latest record.
    """
    return get_last_record(StatisticsData)


def scraped_today() -> bool:
    """
    Check if job statistics were scraped today.

    Returns:
        bool: True if statistics were scraped today, False otherwise.
    """
    last_record: StatisticsData = get_last_record(StatisticsData)

    return last_record.created.date() == date.today() if last_record else False




