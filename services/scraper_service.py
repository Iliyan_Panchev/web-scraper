import re
import time
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException


class ScrapeJobListingsDevBg: 
    """
    Class for scraping job listings from dev.bg.
    """   

    @staticmethod
    def create_search_query(search: str) -> str:
        """
        Accepts a string with spaces and replaces spaces with '+'.
        This is the proper format for searching in URLs.

        Args:
            search (str): The search string.

        Returns:
            str: The formatted search query.
        """
        return '+'.join(search.split(' '))


    @staticmethod
    def _display_job_listings(jobs: dict) -> str:
        """
        Function is used to display information
        formatted for debugging purposes.

        Args:
            jobs (dict): A dictionary of job listings.
        """
        if jobs is None:
            return print('No jobs found.')
        print(f'Jobs on current page: {len(jobs)}\n')
        row = 1
        for title , link in jobs.items():
            print(f'{row}.Title: {title} \n link: {link}\n')
            row += 1
        

    @classmethod
    def get_all_pages(cls, search: str) -> dict[str:str]:
        """
        Collect information from multiple pages when classic pagination is used.

        Args:
            search (str): The search query.

        Returns:
            dict[str, str]: A dictionary with the collected information.
        """
        current_page = 1
        final_result = {}
        while current_page <= 7:
          result = cls.get_job_listings(search, page=current_page)
          if result is None:
            return final_result
          else:
            final_result.update(result)
            current_page += 1  


    @classmethod
    def get_job_listings(cls, search: str ='junior python', page: int=1) -> dict[str:str]:
        """
        Scrape job listings from dev.bg according to a provided search query.

        Args:
            search (str): The search query.
            page (int): The page number.

        Returns:
            dict[str, str]: A dictionary with the collected information.
        """

        base_url = "https://dev.bg/"
        search = cls.create_search_query(search)
        query_params = f'page/{page}/?s={search}&post_type=job_listing#038;post_type=job_listing'
        jobs = {}
        url = f"{base_url}{query_params}"
        print(url)
        response = requests.get(url)

        if response.status_code == 410:
            return None # if response returns status code 410 we exit the function early
     
        soup = BeautifulSoup(response.content, "html.parser")

        job_listings = soup.find_all("div", class_="inner-right listing-content-wrap") #find all containers for job listings

        for listing in job_listings: #iterate through the containers to collect listing name and link
            title: str = listing.find("h6", class_="job-title").text.strip()
            link = listing.find("a", class_="overlay-link")["href"]
            
            if not title:
                continue
            elif 'junior' in search.lower() and 'junior' not in title.lower():
                continue
            elif 'senior' in search.lower() and 'senior' not in title.lower():
                continue
        
            jobs[title + ' - Dev.bg'] = link

        return jobs  
        
 
    

class ScrapeDevBgFrontPage:
    """
    Class for scraping statistics from the front page of dev.bg.
    """
    @classmethod
    def get_job_statistics(cls) -> dict[str:str]:
        """
        Scrapes the front page of Dev.bg, getting all the jobs count.

        Returns:
            dict[str, str]: A dictionary with the job statistics.
        """
        base_url = "https://dev.bg/"
        url = f"{base_url}"
        response = requests.get(url)
        job_listings = {}

        if response.status_code == 200:
            soup = BeautifulSoup(response.content, "html.parser")
            job_statistics = soup.find_all(class_="child-term")

            for html in job_statistics:
                name = html.find('a').text.strip().split()
                job_listings[' '.join(name[:-1])] = name[-1]

        return job_listings   



class ScrapeJobListingsJobsBg:
    """
    Class for scraping job listings from jobs.bg.
    """
    MODAL_CANCEL_XPATH = "//button[@data-mdc-dialog-action='cancel' and .//span[text()='Не']]"
    SCROLLS = 50

    @classmethod
    #@time_it
    def scroll_down_infinite_page(cls, search: str) -> str:
        """
        Collect data from a page with infinite scrolling.

        Args:
            search (str): The search query.

        Returns:
            str: The page as a string.
        """
        search = ScrapeJobListingsDevBg.create_search_query(search) #format the string into acceptable search query format
        base_url = "https://www.jobs.bg/front_job_search.php"
        query_params = f"?subm=1&categories%5B%5D=56&keywords%5B%5D={search}"

        url = f"{base_url}{query_params}" # assemble the url

        options = Options()
        options.add_argument("--headless=new")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--no-sandbox")
        driver = webdriver.Chrome(options=options)
        driver.get(url) #launch selenium in headless mode

        try: # close the window for reccuring search that pop ups if it doesn't handle the exception and continue.
            driver.find_element(By.TAG_NAME,'body').send_keys(Keys.PAGE_DOWN)
            time.sleep(1)
            driver.find_element(By.XPATH, cls.MODAL_CANCEL_XPATH).click()
        except NoSuchElementException:
            print('Window not found.')
        finally:    
            for _ in range(cls.SCROLLS):
                driver.find_element(By.TAG_NAME,'body').send_keys(Keys.PAGE_DOWN)
                #time.sleep(1)          
        return driver.page_source # return page html parsed as string
    

    @classmethod
    #@time_it
    def get_all_pages(cls, search: str) -> dict[str:str]:
        """
        Get job listings from multiple pages with infinite scrolling.

        Args:
            search (str): The search query.

        Returns:
            dict[str, str]: A dictionary with the collected information.
        """
        job_links = {}

        soup = BeautifulSoup(cls.scroll_down_infinite_page(search), "html.parser") # create soup object from the parsed page html
        
        for link in soup.find_all('a', class_='black-link-b'): # search for the container class and extract the name and link 
            title: str = link.get('title')

            if not title:
                continue
            elif 'junior' in search.lower() and 'junior' not in title.lower():
                continue
            elif 'senior' in search.lower() and 'senior' not in title.lower():
                continue

            link_url = link.get('href')

            job_links[title + ' - Jobs.bg'] = link_url #load the dictionary 

        
        return job_links
    
    
