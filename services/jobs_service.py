import os
from core.database.db_connection import insert_data, delete_data, get_last_record, delete_rows_after_count
from core.database.models import Data
from core.database.utilities import get_current_time




def create_new_entry_jobs(data: str):
    """
    Create a new entry in the 'Jobs' table with the provided data.

    Args:
        data (str): The data to be stored in the new entry.

    Returns:
        str: A success message indicating the new entry creation.
    """
    data_entry = Data(data_entry=data, created=get_current_time())
    insert_data(data_entry)

    return f'Successfuly created new entry!'
    

def delete_entry_by_id(id: int):
    """
    Delete an entry from the 'Jobs' table by its ID.

    Args:
        id (int): The ID of the entry to be deleted.

    Returns:
        str: A success message indicating the deletion of the specified entry.
    """
    delete_data(id, Data)
    
    return f'Successfuly deleted entry {id}.'

def get_latest_jobs():
    """
    Retrieve the latest entry from the 'Jobs' table.

    Returns:
        Data | None: The latest entry or None if no entries are found.
    """
    result = get_last_record(Data)

    return result


