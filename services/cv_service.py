import os
import openai
from concurrent.futures import ProcessPoolExecutor
from services.scraper_service import ScrapeJobListingsJobsBg, ScrapeJobListingsDevBg
from openai.error import ServiceUnavailableError

openai.api_key = os.getenv("OPENAI_API_KEY")

class LargeLanguageModelResponse:
  """
  A class for interacting with the OpenAI GPT-3.5-turbo language model to process CV data and scrape job listings.
  """
  PROMPT_READ_CV = '''According to the information from this resume try to guess the position the candite is applying for.
  Reply with the position only, no other text.'''
  # PROMPT_FILTER_RESULTS = '''Return just the jobs that are most suitable for the candidate filter from the dictionary with results.'''

  @classmethod
  def _llm_create_response(cls, prompt: str, data: str) -> str:
    """
    Sends a prompt to the OpenAI API and receives a response as JSON.

    Args:
        prompt (str): The prompt for the language model.
        data (str): The data to be included in the prompt.

    Returns:
        str: The formatted response from the language model.
    """
    try:
      response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "user", 
                  "content": f"{prompt}\n###\n{data}"}],
        temperature=0.5,
        max_tokens=100
        )
    except ServiceUnavailableError as e:
        print(e)
    
    formated_response = response['choices'][0]['message']['content'].strip('\n').strip()
    
    return formated_response
  

  @classmethod
  def compute(cls, cv_data: str) -> dict[str, str]:
      """
      Process CV data using the OpenAI language model and scrape job listings concurrently with multiprocessing.

      Args:
          cv_data (str): The CV data to process.

      Returns:
          dict[str, str]: A dictionary containing the scraped job listings and the search keyword.
      """
      search_keyword = cls._llm_create_response(cls.PROMPT_READ_CV, cv_data) # supply openai with the parsed cv string and prompt

      print(f'Keyword from CV: {search_keyword}')
   
      with ProcessPoolExecutor() as executor: # run the two scrapers concurently using multiprocessing
            future_jobs_bg = executor.submit(ScrapeJobListingsJobsBg.get_all_pages, search_keyword) 
            future_dev_bg = executor.submit(ScrapeJobListingsDevBg.get_all_pages, search_keyword)

            executor.shutdown() 

      scraped_jobs_bg = future_jobs_bg.result() 
      scraped_dev_bg = future_dev_bg.result()

      if scraped_jobs_bg and scraped_dev_bg: #if one the of dicts is empty, proceed to result
          results = {**scraped_jobs_bg, **scraped_dev_bg}
      elif scraped_dev_bg and not scraped_jobs_bg:
          results = scraped_dev_bg
      else:
          results = scraped_jobs_bg
      
      print(f'Jobs found: {len(results)}')

      return results, search_keyword

