import glob
import os
from apscheduler.schedulers.blocking import BlockingScheduler
from controller.helper_functions import clean_download_folder
from core.database.models import Data, StatisticsData
from services import statistics_service, jobs_service
import shutil
scheduler = BlockingScheduler()


@scheduler.scheduled_job('cron', hour=3)
def refresh_data():
    if not statistics_service.scraped_today():
        statistics_service.scrape_statistics()
        statistics_service.scrape_jobs()
        clean_download_folder()
    jobs_service.delete_rows_after_count(count=180, table=StatisticsData)
    jobs_service.delete_rows_after_count(count=5, table=Data)
    

scheduler.start()

