import datetime
from enum import Enum
import time


def get_current_time():
    '''
    Get the current date and time.

    Returns:
        datetime.datetime: The current date and time.
    '''
    return datetime.datetime.now()


def time_it(method):
    '''
    Decorator to measure the execution time of a method.

    Args:
        method (callable): The method to be timed.

    Returns:
        callable: The decorated method.
    '''
    def wrapper(cls, response):
        t1 = time.time()
        result = method(cls, response)
        t2 = time.time() - t1
        print(f'{RAN_IN}{method.__name__} ran in {t2:.3f} seconds')
        return result
    return wrapper


class Color(Enum):
   '''
   Enum defining ANSI color codes for console text formatting. \n
   The text is formatted in the same way uvicorn server messages appear in the console so they blend in.
   '''
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


INFO_TEMPLATE = f'{Color.BLUE.value}INFO{Color.END.value}:     '
DELETED_TEMPLATE = f'{Color.RED.value}INFO{Color.END.value}:     '
RAN_IN = f'{Color.GREEN.value}INFO{Color.END.value}:     '

