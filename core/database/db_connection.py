import os
from sqlalchemy import create_engine, select, delete
from sqlalchemy.orm import sessionmaker
from core.database.models import Base, Data, StatisticsData
from sqlalchemy.dialects import mysql
from core.database.utilities import DELETED_TEMPLATE


db_host = os.getenv('DB_HOST')
db_user = os.getenv('DB_USER')
db_password = os.getenv('DB_PASSWORD')
db_port = os.getenv('DB_PORT')
db_name = os.getenv('MYSQLDATABASE')


engine = create_engine(f'mysql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}', echo=True, dialect=mysql.dialect())

def create_db_session(engine=engine):
    '''
    Create and return a new database session.

    Args:
        engine (Engine, optional): The database engine. Defaults to the global engine.

    Returns:
        Session: The database session.
    '''
    Session = sessionmaker(bind=engine)
    session = Session()

    return session


def create_tables(engine=engine):
    '''
    Create database tables.

    Args:
        engine (Engine, optional): The database engine. Defaults to the global engine.
    '''
    Base.metadata.create_all(bind=engine)
  

def insert_data(data_entry: Data, db_session=create_db_session()):
    '''
    Insert a new data entry into the database.

    Args:
        data_entry (Data): The data entry to insert.
        db_session (Session, optional): The database session. Defaults to a new session.

    Returns:
        None
    '''
    with db_session as sesh:
        sesh.add(data_entry)
        sesh.commit()


def delete_data(data_id: int, table: Data | StatisticsData, db_session=create_db_session()):
    '''
    Delete a data entry from the database.

    Args:
        data_id (int): The ID of the data entry to delete.
        table (Data | StatisticsData): The table to delete from.
        db_session (Session, optional): The database session. Defaults to a new session.

    Returns:
        None
    '''
    record_to_delete = db_session.query(table).get(data_id)
    with db_session as sesh:
        sesh.delete(record_to_delete)
        sesh.commit()


def get_last_record(table: Data | StatisticsData, db_session=create_db_session()):
    '''
    Retrieve the latest record from the specified table.

    Args:
        table (Data | StatisticsData): The table to query.
        db_session (Session, optional): The database session. Defaults to a new session.

    Returns:
        Data | StatisticsData: The latest record.
    '''
    with db_session as sesh:
        results = sesh.query(table).order_by(table.created.desc()).first()

    return results
     

def get_records(number: int, table: Data | StatisticsData, db_session=create_db_session()):
    '''
    Retrieve a specified number of records from the specified table.

    Args:
        number (int): The number of records to retrieve.
        table (Data | StatisticsData): The table to query.
        db_session (Session, optional): The database session. Defaults to a new session.

    Returns:
        List[Data | StatisticsData]: The retrieved records.
    '''
    with db_session as sesh:
        results = sesh.query(table).order_by(table.created.desc()).limit(number)[::-1]
    
    return results


def rows_count(table: Data | StatisticsData, db_session=create_db_session()):
    '''
    Get the count of rows in the specified table.

    Args:
        table (Data | StatisticsData): The table to query.
        db_session (Session, optional): The database session. Defaults to a new session.

    Returns:
        int: The count of rows.
    '''
    with db_session as sesh:
        rows_count = sesh.query(table).count()

    return rows_count    


def delete_rows_after_count(count: int, table: Data | StatisticsData,  db_session=create_db_session()):
    '''
    Delete rows from the specified table to keep a specified count. \n
    This function is specifically used in the automation script named clock.py

    Args:
        count (int): The number of rows to keep.
        table (Data | StatisticsData): The table to query.
        db_session (Session, optional): The database session. Defaults to a new session.

    Returns:
        None
    '''
    row_count = rows_count(table=table)

    rows_to_keep = max(0, row_count - count)

    if rows_to_keep < row_count:
        with db_session as sesh:
            subquery = select(table.id).order_by(table.created.desc()).limit(count).subquery()
            sesh.execute(delete(table).where(table.id.notin_(subquery)))
            sesh.commit()
    print(DELETED_TEMPLATE + f'Rows deleted {row_count - rows_to_keep}.')


