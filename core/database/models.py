
from sqlalchemy import JSON, Column, Integer, DateTime
from sqlalchemy.orm import DeclarativeBase



class Base(DeclarativeBase):
      ...     


class Data(Base):
      '''
      Model for the Jobs table.
      '''
      __tablename__ = 'Jobs'

      id = Column('id', Integer, autoincrement=True, primary_key=True)
      data_entry =  Column(JSON)
      created = Column('created', DateTime)


class StatisticsData(Base):
      '''
      Model for the Jobs_statistics table.
      '''
      __tablename__ = 'Jobs_statistics'

      id = Column('id', Integer, autoincrement=True, primary_key=True)
      data_entry = Column(JSON)
      created = Column('created', DateTime)


